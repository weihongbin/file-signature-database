# FileSignatureDatabase

#### 介绍
文件签名数据库
数据来自 https://filesignatures.net/index.php?page=all  

#### 软件架构

**FileSignatureCrawlingUtil.java**  
- Java 从 filesignatures.net 获取文件签名数据库源码
- 生成 FileSignatureDatabase.md 文件

**GenerateFileSignatureUtil.java**
- 生成 FileSignatureUtil 文件

**FileSignatureUtil.java**
- check 方法进行文件头检查校验


#### 使用示例

```java
public static void main(String[] args) throws IOException {
    int check = FileSignatureUtil.check(new File("Git-2.30.0.2-64-bit.exe"));
    System.out.println("检查结果："+check);
}
```

#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
