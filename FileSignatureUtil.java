import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * 文件头检查工具
 *
 * @author WeiHongBin
 */
public class FileSignatureUtil {
    private static final Map<String, byte[][]> FILE_SIGNATURE_MAP = new HashMap<>();

    static {
        // * ['AOL parameter','Binary property list (plist)','BIOS details in RAM','cpio archive','ELF executable','Extended tcpdump (libpcap) capture file','INFO2 Windows recycle bin_1','INFO2 Windows recycle bin_2','Java serialization data','KWAJ (compressed) file','NAV quarantined virus file','QBASIC SZDD file','SMS text (SIM)','SZDD file format','tcpdump (libpcap) capture file','Tcpdump capture file','UTF8 file','UTF-16','UTF-32','UUencoded file','WinDump (winpcap) capture file','zisofs compressed file']
        byte[][] extAsterisk = {{0x41, 0x43, 0x53, 0x44}, {0x62, 0x70, 0x6C, 0x69, 0x73, 0x74}, {0x00, 0x14, 0x00, 0x00, 0x01, 0x02}, {0x30, 0x37, 0x30, 0x37, 0x30}, {0x7F, 0x45, 0x4C, 0x46}, {(byte) 0xA1, (byte) 0xB2, (byte) 0xCD, 0x34}, {0x04, 0x00, 0x00, 0x00}, {0x05, 0x00, 0x00, 0x00}, {(byte) 0xAC, (byte) 0xED}, {0x4B, 0x57, 0x41, 0x4A, (byte) 0x88, (byte) 0xF0, 0x27, (byte) 0xD1}, {(byte) 0xCD, 0x20, (byte) 0xAA, (byte) 0xAA, 0x02, 0x00, 0x00, 0x00}, {0x53, 0x5A, 0x20, (byte) 0x88, (byte) 0xF0, 0x27, 0x33, (byte) 0xD1}, {0x6F, 0x3C}, {0x53, 0x5A, 0x44, 0x44, (byte) 0x88, (byte) 0xF0, 0x27, 0x33}, {(byte) 0xA1, (byte) 0xB2, (byte) 0xC3, (byte) 0xD4}, {0x34, (byte) 0xCD, (byte) 0xB2, (byte) 0xA1}, {(byte) 0xEF, (byte) 0xBB, (byte) 0xBF}, {(byte) 0xFE, (byte) 0xFF}, {(byte) 0xFF, (byte) 0xFE, 0x00, 0x00}, {0x62, 0x65, 0x67, 0x69, 0x6E}, {(byte) 0xD4, (byte) 0xC3, (byte) 0xB2, (byte) 0xA1}, {0x37, (byte) 0xE4, 0x53, (byte) 0x96, (byte) 0xC9, (byte) 0xDB, (byte) 0xD6, 0x07}};
        FILE_SIGNATURE_MAP.put("*", extAsterisk);
        // 123 ['Lotus 1-2-3 (v9)']
        byte[][] ext123 = {{0x00, 0x00, 0x1A, 0x00, 0x05, 0x10, 0x04}};
        FILE_SIGNATURE_MAP.put("123", ext123);
        // 386 ['Windows virtual device drivers']
        byte[][] ext386 = {{0x4D, 0x5A}};
        FILE_SIGNATURE_MAP.put("386", ext386);
        // 3GP ['3GPP multimedia files','3GPP2 multimedia files']
        byte[][] ext3GP = {{0x00, 0x00, 0x00, 0x14, 0x66, 0x74, 0x79, 0x70}, {0x00, 0x00, 0x00, 0x20, 0x66, 0x74, 0x79, 0x70}};
        FILE_SIGNATURE_MAP.put("3GP", ext3GP);
        // 3GP5 ['MPEG-4 video files']
        byte[][] ext3GP5 = {{0x00, 0x00, 0x00, 0x18, 0x66, 0x74, 0x79, 0x70}};
        FILE_SIGNATURE_MAP.put("3GP5", ext3GP5);
        // 4XM ['4X Movie video']
        byte[][] ext4XM = {{0x52, 0x49, 0x46, 0x46}};
        FILE_SIGNATURE_MAP.put("4XM", ext4XM);
        // 7Z ['7-Zip compressed file']
        byte[][] ext7Z = {{0x37, 0x7A, (byte) 0xBC, (byte) 0xAF, 0x27, 0x1C}};
        FILE_SIGNATURE_MAP.put("7Z", ext7Z);
        // ABA ['Palm Address Book Archive']
        byte[][] extABA = {{0x00, 0x01, 0x42, 0x41}};
        FILE_SIGNATURE_MAP.put("ABA", extABA);
        // ABD ['ABD']
        byte[][] extABD = {{0x51, 0x57, 0x20, 0x56, 0x65, 0x72, 0x2E, 0x20}};
        FILE_SIGNATURE_MAP.put("ABD", extABD);
        // ABI ['AOL address book index','AOL config files']
        byte[][] extABI = {{0x41, 0x4F, 0x4C, 0x49, 0x4E, 0x44, 0x45, 0x58}, {0x41, 0x4F, 0x4C}};
        FILE_SIGNATURE_MAP.put("ABI", extABI);
        // ABY ['AOL address book','AOL config files']
        byte[][] extABY = {{0x41, 0x4F, 0x4C, 0x44, 0x42}, {0x41, 0x4F, 0x4C}};
        FILE_SIGNATURE_MAP.put("ABY", extABY);
        // AC ['Sonic Foundry Acid Music File']
        byte[][] extAC = {{0x72, 0x69, 0x66, 0x66}};
        FILE_SIGNATURE_MAP.put("AC", extAC);
        // ACCDB ['Microsoft Access 2007']
        byte[][] extACCDB = {{0x00, 0x01, 0x00, 0x00, 0x53, 0x74, 0x61, 0x6E, 0x64, 0x61, 0x72, 0x64, 0x20, 0x41, 0x43, 0x45, 0x20, 0x44, 0x42}};
        FILE_SIGNATURE_MAP.put("ACCDB", extACCDB);
        // ACM ['MS audio compression manager driver']
        byte[][] extACM = {{0x4D, 0x5A}};
        FILE_SIGNATURE_MAP.put("ACM", extACM);
        // ACS ['MS Agent Character file']
        byte[][] extACS = {{(byte) 0xC3, (byte) 0xAB, (byte) 0xCD, (byte) 0xAB}};
        FILE_SIGNATURE_MAP.put("ACS", extACS);
        // AC_ ['CaseWare Working Papers']
        byte[][] extAC_ = {{(byte) 0xD0, (byte) 0xCF, 0x11, (byte) 0xE0, (byte) 0xA1, (byte) 0xB1, 0x1A, (byte) 0xE1}};
        FILE_SIGNATURE_MAP.put("AC_", extAC_);
        // AD ['Antenna data file']
        byte[][] extAD = {{0x52, 0x45, 0x56, 0x4E, 0x55, 0x4D, 0x3A, 0x2C}};
        FILE_SIGNATURE_MAP.put("AD", extAD);
        // ADF ['Amiga disk file']
        byte[][] extADF = {{0x44, 0x4F, 0x53}};
        FILE_SIGNATURE_MAP.put("ADF", extADF);
        // ADP ['Access project file']
        byte[][] extADP = {{(byte) 0xD0, (byte) 0xCF, 0x11, (byte) 0xE0, (byte) 0xA1, (byte) 0xB1, 0x1A, (byte) 0xE1}};
        FILE_SIGNATURE_MAP.put("ADP", extADP);
        // ADX ['Approach index file','Dreamcast audio']
        byte[][] extADX = {{0x03, 0x00, 0x00, 0x00, 0x41, 0x50, 0x50, 0x52}, {(byte) 0x80, 0x00, 0x00, 0x20, 0x03, 0x12, 0x04}};
        FILE_SIGNATURE_MAP.put("ADX", extADX);
        // AIFF ['Audio Interchange File']
        byte[][] extAIFF = {{0x46, 0x4F, 0x52, 0x4D, 0x00}};
        FILE_SIGNATURE_MAP.put("AIFF", extAIFF);
        // AIN ['AIN Compressed Archive']
        byte[][] extAIN = {{0x21, 0x12}};
        FILE_SIGNATURE_MAP.put("AIN", extAIN);
        // AMR ['Adaptive Multi-Rate ACELP Codec (GSM)']
        byte[][] extAMR = {{0x23, 0x21, 0x41, 0x4D, 0x52}};
        FILE_SIGNATURE_MAP.put("AMR", extAMR);
        // ANI ['Windows animated cursor']
        byte[][] extANI = {{0x52, 0x49, 0x46, 0x46}};
        FILE_SIGNATURE_MAP.put("ANI", extANI);
        // API ['Acrobat plug-in']
        byte[][] extAPI = {{0x4D, 0x5A, (byte) 0x90, 0x00, 0x03, 0x00, 0x00, 0x00}};
        FILE_SIGNATURE_MAP.put("API", extAPI);
        // APR ['Lotus']
        byte[][] extAPR = {{(byte) 0xD0, (byte) 0xCF, 0x11, (byte) 0xE0, (byte) 0xA1, (byte) 0xB1, 0x1A, (byte) 0xE1}};
        FILE_SIGNATURE_MAP.put("APR", extAPR);
        // ARC ['FreeArc compressed file','LH archive (old vers.','LH archive (old vers.','LH archive (old vers.','LH archive (old vers.','LH archive (old vers.']
        byte[][] extARC = {{0x41, 0x72, 0x43, 0x01}, {0x1A, 0x02}, {0x1A, 0x03}, {0x1A, 0x04}, {0x1A, 0x08}, {0x1A, 0x09}};
        FILE_SIGNATURE_MAP.put("ARC", extARC);
        // ARJ ['ARJ Compressed archive file']
        byte[][] extARJ = {{0x60, (byte) 0xEA}};
        FILE_SIGNATURE_MAP.put("ARJ", extARJ);
        // ARL ['AOL history']
        byte[][] extARL = {{(byte) 0xD4, 0x2A}};
        FILE_SIGNATURE_MAP.put("ARL", extARL);
        // ASF ['Windows Media Audio']
        byte[][] extASF = {{0x30, 0x26, (byte) 0xB2, 0x75, (byte) 0x8E, 0x66, (byte) 0xCF, 0x11}};
        FILE_SIGNATURE_MAP.put("ASF", extASF);
        // AST ['Underground Audio']
        byte[][] extAST = {{0x53, 0x43, 0x48, 0x6C}};
        FILE_SIGNATURE_MAP.put("AST", extAST);
        // ASX ['Advanced Stream Redirector']
        byte[][] extASX = {{0x3C}};
        FILE_SIGNATURE_MAP.put("ASX", extASX);
        // AU ['Audacity audio file','NeXT']
        byte[][] extAU = {{0x64, 0x6E, 0x73, 0x2E}, {0x2E, 0x73, 0x6E, 0x64}};
        FILE_SIGNATURE_MAP.put("AU", extAU);
        // AUT ['AOL history']
        byte[][] extAUT = {{(byte) 0xD4, 0x2A}};
        FILE_SIGNATURE_MAP.put("AUT", extAUT);
        // AVI ['Resource Interchange File Format']
        byte[][] extAVI = {{0x52, 0x49, 0x46, 0x46}};
        FILE_SIGNATURE_MAP.put("AVI", extAVI);
        // AW ['MS Answer Wizard']
        byte[][] extAW = {{(byte) 0x8A, 0x01, 0x09, 0x00, 0x00, 0x00, (byte) 0xE1, 0x08}};
        FILE_SIGNATURE_MAP.put("AW", extAW);
        // AX ['DirectShow filter','Library cache file']
        byte[][] extAX = {{0x4D, 0x5A, (byte) 0x90, 0x00, 0x03, 0x00, 0x00, 0x00}, {0x4D, 0x5A}};
        FILE_SIGNATURE_MAP.put("AX", extAX);
        // BAG ['AOL and AIM buddy list','AOL config files']
        byte[][] extBAG = {{0x41, 0x4F, 0x4C, 0x20, 0x46, 0x65, 0x65, 0x64}, {0x41, 0x4F, 0x4C}};
        FILE_SIGNATURE_MAP.put("BAG", extBAG);
        // BDR ['MS Publisher']
        byte[][] extBDR = {{0x58, 0x54}};
        FILE_SIGNATURE_MAP.put("BDR", extBDR);
        // BIN ['Speedtouch router firmware']
        byte[][] extBIN = {{0x42, 0x4C, 0x49, 0x32, 0x32, 0x33, 0x51}};
        FILE_SIGNATURE_MAP.put("BIN", extBIN);
        // BMP ['Bitmap image']
        byte[][] extBMP = {{0x42, 0x4D}};
        FILE_SIGNATURE_MAP.put("BMP", extBMP);
        // BZ2 ['bzip2 compressed archive']
        byte[][] extBZ2 = {{0x42, 0x5A, 0x68}};
        FILE_SIGNATURE_MAP.put("BZ2", extBZ2);
        // CAB ['Install Shield compressed file','Microsoft cabinet file']
        byte[][] extCAB = {{0x49, 0x53, 0x63, 0x28}, {0x4D, 0x53, 0x43, 0x46}};
        FILE_SIGNATURE_MAP.put("CAB", extCAB);
        // CAL ['CALS raster bitmap','SuperCalc worksheet','Windows calendar']
        byte[][] extCAL = {{0x73, 0x72, 0x63, 0x64, 0x6F, 0x63, 0x69, 0x64}, {0x53, 0x75, 0x70, 0x65, 0x72, 0x43, 0x61, 0x6C}, {(byte) 0xB5, (byte) 0xA2, (byte) 0xB0, (byte) 0xB3, (byte) 0xB3, (byte) 0xB0, (byte) 0xA5, (byte) 0xB5}};
        FILE_SIGNATURE_MAP.put("CAL", extCAL);
        // CAP ['Packet sniffer files','WinNT Netmon capture file']
        byte[][] extCAP = {{0x58, 0x43, 0x50, 0x00}, {0x52, 0x54, 0x53, 0x53}};
        FILE_SIGNATURE_MAP.put("CAP", extCAP);
        // CAS ['EnCase case file']
        byte[][] extCAS = {{0x5F, 0x43, 0x41, 0x53, 0x45, 0x5F}};
        FILE_SIGNATURE_MAP.put("CAS", extCAS);
        // CAT ['MS security catalog file']
        byte[][] extCAT = {{0x30}};
        FILE_SIGNATURE_MAP.put("CAT", extCAT);
        // CBD ['WordPerfect dictionary']
        byte[][] extCBD = {{0x43, 0x42, 0x46, 0x49, 0x4C, 0x45}};
        FILE_SIGNATURE_MAP.put("CBD", extCBD);
        // CBK ['EnCase case file']
        byte[][] extCBK = {{0x5F, 0x43, 0x41, 0x53, 0x45, 0x5F}};
        FILE_SIGNATURE_MAP.put("CBK", extCBK);
        // CDA ['Resource Interchange File Format']
        byte[][] extCDA = {{0x52, 0x49, 0x46, 0x46}};
        FILE_SIGNATURE_MAP.put("CDA", extCDA);
        // CDR ['CorelDraw document','Elite Plus Commander game file','Sony Compressed Voice File']
        byte[][] extCDR = {{0x52, 0x49, 0x46, 0x46}, {0x45, 0x4C, 0x49, 0x54, 0x45, 0x20, 0x43, 0x6F}, {0x4D, 0x53, 0x5F, 0x56, 0x4F, 0x49, 0x43, 0x45}};
        FILE_SIGNATURE_MAP.put("CDR", extCDR);
        // CFG ['Flight Simulator Aircraft Configuration']
        byte[][] extCFG = {{0x5B, 0x66, 0x6C, 0x74, 0x73, 0x69, 0x6D, 0x2E}};
        FILE_SIGNATURE_MAP.put("CFG", extCFG);
        // CHI ['MS Compiled HTML Help File']
        byte[][] extCHI = {{0x49, 0x54, 0x53, 0x46}};
        FILE_SIGNATURE_MAP.put("CHI", extCHI);
        // CHM ['MS Compiled HTML Help File']
        byte[][] extCHM = {{0x49, 0x54, 0x53, 0x46}};
        FILE_SIGNATURE_MAP.put("CHM", extCHM);
        // CLASS ['Java bytecode']
        byte[][] extCLASS = {{(byte) 0xCA, (byte) 0xFE, (byte) 0xBA, (byte) 0xBE}};
        FILE_SIGNATURE_MAP.put("CLASS", extCLASS);
        // CLB ['COM+ Catalog','Corel Binary metafile']
        byte[][] extCLB = {{0x43, 0x4F, 0x4D, 0x2B}, {0x43, 0x4D, 0x58, 0x31}};
        FILE_SIGNATURE_MAP.put("CLB", extCLB);
        // CMX ['Corel Presentation Exchange metadata']
        byte[][] extCMX = {{0x52, 0x49, 0x46, 0x46}};
        FILE_SIGNATURE_MAP.put("CMX", extCMX);
        // CNV ['DB2 conversion file']
        byte[][] extCNV = {{0x53, 0x51, 0x4C, 0x4F, 0x43, 0x4F, 0x4E, 0x56}};
        FILE_SIGNATURE_MAP.put("CNV", extCNV);
        // COD ['Agent newsreader character map']
        byte[][] extCOD = {{0x4E, 0x61, 0x6D, 0x65, 0x3A, 0x20}};
        FILE_SIGNATURE_MAP.put("COD", extCOD);
        // COM ['Windows','Windows executable file_1','Windows executable file_2','Windows executable file_3']
        byte[][] extCOM = {{0x4D, 0x5A}, {(byte) 0xE8}, {(byte) 0xE9}, {(byte) 0xEB}};
        FILE_SIGNATURE_MAP.put("COM", extCOM);
        // CPE ['MS Fax Cover Sheet']
        byte[][] extCPE = {{0x46, 0x41, 0x58, 0x43, 0x4F, 0x56, 0x45, 0x52}};
        FILE_SIGNATURE_MAP.put("CPE", extCPE);
        // CPI ['Sietronics CPI XRD document','Windows international code page']
        byte[][] extCPI = {{0x53, 0x49, 0x45, 0x54, 0x52, 0x4F, 0x4E, 0x49}, {(byte) 0xFF, 0x46, 0x4F, 0x4E, 0x54}};
        FILE_SIGNATURE_MAP.put("CPI", extCPI);
        // CPL ['Control panel application','Corel color palette']
        byte[][] extCPL = {{0x4D, 0x5A}, {(byte) 0xDC, (byte) 0xDC}};
        FILE_SIGNATURE_MAP.put("CPL", extCPL);
        // CPT ['Corel Photopaint file_1','Corel Photopaint file_2']
        byte[][] extCPT = {{0x43, 0x50, 0x54, 0x37, 0x46, 0x49, 0x4C, 0x45}, {0x43, 0x50, 0x54, 0x46, 0x49, 0x4C, 0x45}};
        FILE_SIGNATURE_MAP.put("CPT", extCPT);
        // CPX ['Microsoft Code Page Translation file']
        byte[][] extCPX = {{0x5B, 0x57, 0x69, 0x6E, 0x64, 0x6F, 0x77, 0x73}};
        FILE_SIGNATURE_MAP.put("CPX", extCPX);
        // CRU ['Crush compressed archive']
        byte[][] extCRU = {{0x43, 0x52, 0x55, 0x53, 0x48, 0x20, 0x76}};
        FILE_SIGNATURE_MAP.put("CRU", extCRU);
        // CRW ['Canon RAW file']
        byte[][] extCRW = {{0x49, 0x49, 0x1A, 0x00, 0x00, 0x00, 0x48, 0x45}};
        FILE_SIGNATURE_MAP.put("CRW", extCRW);
        // CSH ['Photoshop Custom Shape']
        byte[][] extCSH = {{0x63, 0x75, 0x73, 0x68, 0x00, 0x00, 0x00, 0x02}};
        FILE_SIGNATURE_MAP.put("CSH", extCSH);
        // CTF ['WhereIsIt Catalog']
        byte[][] extCTF = {{0x43, 0x61, 0x74, 0x61, 0x6C, 0x6F, 0x67, 0x20}};
        FILE_SIGNATURE_MAP.put("CTF", extCTF);
        // CTL ['Visual Basic User-defined Control file']
        byte[][] extCTL = {{0x56, 0x45, 0x52, 0x53, 0x49, 0x4F, 0x4E, 0x20}};
        FILE_SIGNATURE_MAP.put("CTL", extCTL);
        // CUIX ['Customization files']
        byte[][] extCUIX = {{0x50, 0x4B, 0x03, 0x04}};
        FILE_SIGNATURE_MAP.put("CUIX", extCUIX);
        // CUR ['Windows cursor']
        byte[][] extCUR = {{0x00, 0x00, 0x02, 0x00}};
        FILE_SIGNATURE_MAP.put("CUR", extCUR);
        // DAT ['Video CD MPEG movie','Access Data FTK evidence','Allegro Generic Packfile (compressed)','Allegro Generic Packfile (uncompressed)','AVG6 Integrity database','MapInfo Native Data Format','EasyRecovery Saved State file','IE History file','Inno Setup Uninstall Log','Norton Disk Doctor undo file','PestPatrol data','Runtime Software disk image','Shareaza (P2P) thumbnail','TomTom traffic data','UFO Capture map file','Walkman MP3 file','Win9x registry hive','WinNT registry file']
        byte[][] extDAT = {{0x52, 0x49, 0x46, 0x46}, {(byte) 0xA9, 0x0D, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}, {0x73, 0x6C, 0x68, 0x21}, {0x73, 0x6C, 0x68, 0x2E}, {0x41, 0x56, 0x47, 0x36, 0x5F, 0x49, 0x6E, 0x74}, {0x03}, {0x45, 0x52, 0x46, 0x53, 0x53, 0x41, 0x56, 0x45}, {0x43, 0x6C, 0x69, 0x65, 0x6E, 0x74, 0x20, 0x55}, {0x49, 0x6E, 0x6E, 0x6F, 0x20, 0x53, 0x65, 0x74}, {0x50, 0x4E, 0x43, 0x49, 0x55, 0x4E, 0x44, 0x4F}, {0x50, 0x45, 0x53, 0x54}, {0x1A, 0x52, 0x54, 0x53, 0x20, 0x43, 0x4F, 0x4D}, {0x52, 0x41, 0x5A, 0x41, 0x54, 0x44, 0x42, 0x31}, {0x4E, 0x41, 0x56, 0x54, 0x52, 0x41, 0x46, 0x46}, {0x55, 0x46, 0x4F, 0x4F, 0x72, 0x62, 0x69, 0x74}, {0x57, 0x4D, 0x4D, 0x50}, {0x43, 0x52, 0x45, 0x47}, {0x72, 0x65, 0x67, 0x66}};
        FILE_SIGNATURE_MAP.put("DAT", extDAT);
        // DB ['MSWorks database file','dBASE IV or dBFast configuration file','Netscape Navigator (v4) database','Palm Zire photo database','SQLite database file','Thumbs.db subheader']
        byte[][] extDB = {{(byte) 0xD0, (byte) 0xCF, 0x11, (byte) 0xE0, (byte) 0xA1, (byte) 0xB1, 0x1A, (byte) 0xE1}, {0x08}, {0x00, 0x06, 0x15, 0x61, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x04, (byte) 0xD2, 0x00, 0x00, 0x10, 0x00}, {0x44, 0x42, 0x46, 0x48}, {0x53, 0x51, 0x4C, 0x69, 0x74, 0x65, 0x20, 0x66, 0x6F, 0x72, 0x6D, 0x61, 0x74, 0x20, 0x33, 0x00}, {(byte) 0xFD, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF}};
        FILE_SIGNATURE_MAP.put("DB", extDB);
        // DB3 ['dBASE III file']
        byte[][] extDB3 = {{0x03}};
        FILE_SIGNATURE_MAP.put("DB3", extDB3);
        // DB4 ['dBASE IV file']
        byte[][] extDB4 = {{0x04}};
        FILE_SIGNATURE_MAP.put("DB4", extDB4);
        // DBA ['Palm DateBook Archive']
        byte[][] extDBA = {{0x00, 0x01, 0x42, 0x44}};
        FILE_SIGNATURE_MAP.put("DBA", extDBA);
        // DBB ['Skype user data file']
        byte[][] extDBB = {{0x6C, 0x33, 0x33, 0x6C}};
        FILE_SIGNATURE_MAP.put("DBB", extDBB);
        // DBF ['Psion Series 3 Database']
        byte[][] extDBF = {{0x4F, 0x50, 0x4C, 0x44, 0x61, 0x74, 0x61, 0x62}};
        FILE_SIGNATURE_MAP.put("DBF", extDBF);
        // DBX ['Outlook Express e-mail folder']
        byte[][] extDBX = {{(byte) 0xCF, (byte) 0xAD, 0x12, (byte) 0xFE}};
        FILE_SIGNATURE_MAP.put("DBX", extDBX);
        // DCI ['AOL HTML mail']
        byte[][] extDCI = {{0x3C, 0x21, 0x64, 0x6F, 0x63, 0x74, 0x79, 0x70}};
        FILE_SIGNATURE_MAP.put("DCI", extDCI);
        // DCX ['PCX bitmap']
        byte[][] extDCX = {{(byte) 0xB1, 0x68, (byte) 0xDE, 0x3A}};
        FILE_SIGNATURE_MAP.put("DCX", extDCX);
        // dex ['Dalvik (Android) executable file']
        byte[][] extdex = {{0x64, 0x65, 0x78, 0x0A, 0x30, 0x30, 0x39, 0x00}};
        FILE_SIGNATURE_MAP.put("dex", extdex);
        // DIB ['Bitmap image']
        byte[][] extDIB = {{0x42, 0x4D}};
        FILE_SIGNATURE_MAP.put("DIB", extDIB);
        // DLL ['Windows']
        byte[][] extDLL = {{0x4D, 0x5A}};
        FILE_SIGNATURE_MAP.put("DLL", extDLL);
        // DMG ['MacOS X image file']
        byte[][] extDMG = {{0x78}};
        FILE_SIGNATURE_MAP.put("DMG", extDMG);
        // DMP ['Windows dump file','Windows memory dump']
        byte[][] extDMP = {{0x4D, 0x44, 0x4D, 0x50, (byte) 0x93, (byte) 0xA7}, {0x50, 0x41, 0x47, 0x45, 0x44, 0x55}};
        FILE_SIGNATURE_MAP.put("DMP", extDMP);
        // DMS ['Amiga DiskMasher compressed archive']
        byte[][] extDMS = {{0x44, 0x4D, 0x53, 0x21}};
        FILE_SIGNATURE_MAP.put("DMS", extDMS);
        // DOC ['Microsoft Office document','DeskMate Document','Perfect Office document','Word 2.0 file','Word document subheader']
        byte[][] extDOC = {{(byte) 0xD0, (byte) 0xCF, 0x11, (byte) 0xE0, (byte) 0xA1, (byte) 0xB1, 0x1A, (byte) 0xE1}, {0x0D, 0x44, 0x4F, 0x43}, {(byte) 0xCF, 0x11, (byte) 0xE0, (byte) 0xA1, (byte) 0xB1, 0x1A, (byte) 0xE1, 0x00}, {(byte) 0xDB, (byte) 0xA5, 0x2D, 0x00}, {(byte) 0xEC, (byte) 0xA5, (byte) 0xC1, 0x00}};
        FILE_SIGNATURE_MAP.put("DOC", extDOC);
        // DOCX ['MS Office Open XML Format Document','MS Office 2007 documents']
        byte[][] extDOCX = {{0x50, 0x4B, 0x03, 0x04}, {0x50, 0x4B, 0x03, 0x04, 0x14, 0x00, 0x06, 0x00}};
        FILE_SIGNATURE_MAP.put("DOCX", extDOCX);
        // DOT ['Microsoft Office document']
        byte[][] extDOT = {{(byte) 0xD0, (byte) 0xCF, 0x11, (byte) 0xE0, (byte) 0xA1, (byte) 0xB1, 0x1A, (byte) 0xE1}};
        FILE_SIGNATURE_MAP.put("DOT", extDOT);
        // DRV ['Windows']
        byte[][] extDRV = {{0x4D, 0x5A}};
        FILE_SIGNATURE_MAP.put("DRV", extDRV);
        // DRW ['Generic drawing programs','Micrografx vector graphic file']
        byte[][] extDRW = {{0x07}, {0x01, (byte) 0xFF, 0x02, 0x04, 0x03, 0x02}};
        FILE_SIGNATURE_MAP.put("DRW", extDRW);
        // DS4 ['Micrografx Designer graphic']
        byte[][] extDS4 = {{0x52, 0x49, 0x46, 0x46}};
        FILE_SIGNATURE_MAP.put("DS4", extDS4);
        // DSN ['CD Stomper Pro label file']
        byte[][] extDSN = {{0x4D, 0x56}};
        FILE_SIGNATURE_MAP.put("DSN", extDSN);
        // DSP ['MS Developer Studio project file']
        byte[][] extDSP = {{0x23, 0x20, 0x4D, 0x69, 0x63, 0x72, 0x6F, 0x73}};
        FILE_SIGNATURE_MAP.put("DSP", extDSP);
        // DSS ['Digital Speech Standard file']
        byte[][] extDSS = {{0x02, 0x64, 0x73, 0x73}};
        FILE_SIGNATURE_MAP.put("DSS", extDSS);
        // DSW ['MS Visual Studio workspace file']
        byte[][] extDSW = {{0x64, 0x73, 0x77, 0x66, 0x69, 0x6C, 0x65}};
        FILE_SIGNATURE_MAP.put("DSW", extDSW);
        // DTD ['DesignTools 2D Design file']
        byte[][] extDTD = {{0x07, 0x64, 0x74, 0x32, 0x64, 0x64, 0x74, 0x64}};
        FILE_SIGNATURE_MAP.put("DTD", extDTD);
        // DUN ['Dial-up networking file']
        byte[][] extDUN = {{0x5B, 0x50, 0x68, 0x6F, 0x6E, 0x65, 0x5D}};
        FILE_SIGNATURE_MAP.put("DUN", extDUN);
        // DVF ['Sony Compressed Voice File']
        byte[][] extDVF = {{0x4D, 0x53, 0x5F, 0x56, 0x4F, 0x49, 0x43, 0x45}};
        FILE_SIGNATURE_MAP.put("DVF", extDVF);
        // DVR ['DVR-Studio stream file']
        byte[][] extDVR = {{0x44, 0x56, 0x44}};
        FILE_SIGNATURE_MAP.put("DVR", extDVR);
        // DW4 ['Visio']
        byte[][] extDW4 = {{0x4F, 0x7B}};
        FILE_SIGNATURE_MAP.put("DW4", extDW4);
        // DWG ['Generic AutoCAD drawing']
        byte[][] extDWG = {{0x41, 0x43, 0x31, 0x30}};
        FILE_SIGNATURE_MAP.put("DWG", extDWG);
        // E01 ['Expert Witness Compression Format','Logical File Evidence Format']
        byte[][] extE01 = {{0x45, 0x56, 0x46, 0x09, 0x0D, 0x0A, (byte) 0xFF, 0x00}, {0x4C, 0x56, 0x46, 0x09, 0x0D, 0x0A, (byte) 0xFF, 0x00}};
        FILE_SIGNATURE_MAP.put("E01", extE01);
        // ECF ['MS Exchange configuration file']
        byte[][] extECF = {{0x5B, 0x47, 0x65, 0x6E, 0x65, 0x72, 0x61, 0x6C}};
        FILE_SIGNATURE_MAP.put("ECF", extECF);
        // EFX ['eFax file']
        byte[][] extEFX = {{(byte) 0xDC, (byte) 0xFE}};
        FILE_SIGNATURE_MAP.put("EFX", extEFX);
        // EML ['Exchange e-mail','Generic e-mail_1','Generic e-mail_2']
        byte[][] extEML = {{0x58, 0x2D}, {0x52, 0x65, 0x74, 0x75, 0x72, 0x6E, 0x2D, 0x50}, {0x46, 0x72, 0x6F, 0x6D}};
        FILE_SIGNATURE_MAP.put("EML", extEML);
        // ENL ['EndNote Library File']
        byte[][] extENL = {{0x40, 0x40, 0x40, 0x20, 0x00, 0x00, 0x40, 0x40, 0x40, 0x40}};
        FILE_SIGNATURE_MAP.put("ENL", extENL);
        // EPS ['Adobe encapsulated PostScript','Encapsulated PostScript file']
        byte[][] extEPS = {{(byte) 0xC5, (byte) 0xD0, (byte) 0xD3, (byte) 0xC6}, {0x25, 0x21, 0x50, 0x53, 0x2D, 0x41, 0x64, 0x6F}};
        FILE_SIGNATURE_MAP.put("EPS", extEPS);
        // ETH ['WinPharoah capture file']
        byte[][] extETH = {{0x1A, 0x35, 0x01, 0x00}};
        FILE_SIGNATURE_MAP.put("ETH", extETH);
        // EVT ['Windows Event Viewer file']
        byte[][] extEVT = {{0x30, 0x00, 0x00, 0x00, 0x4C, 0x66, 0x4C, 0x65}};
        FILE_SIGNATURE_MAP.put("EVT", extEVT);
        // EVTX ['Windows Vista event log']
        byte[][] extEVTX = {{0x45, 0x6C, 0x66, 0x46, 0x69, 0x6C, 0x65, 0x00}};
        FILE_SIGNATURE_MAP.put("EVTX", extEVTX);
        // EXE ['Windows']
        byte[][] extEXE = {{0x4D, 0x5A}};
        FILE_SIGNATURE_MAP.put("EXE", extEXE);
        // FDF ['PDF file']
        byte[][] extFDF = {{0x25, 0x50, 0x44, 0x46}};
        FILE_SIGNATURE_MAP.put("FDF", extFDF);
        // FLAC ['Free Lossless Audio Codec file']
        byte[][] extFLAC = {{0x66, 0x4C, 0x61, 0x43, 0x00, 0x00, 0x00, 0x22}};
        FILE_SIGNATURE_MAP.put("FLAC", extFLAC);
        // FLI ['FLIC animation']
        byte[][] extFLI = {{0x00, 0x11}};
        FILE_SIGNATURE_MAP.put("FLI", extFLI);
        // FLT ['Audition graphic filter','Qimage filter']
        byte[][] extFLT = {{0x4D, 0x5A, (byte) 0x90, 0x00, 0x03, 0x00, 0x00, 0x00}, {0x76, 0x32, 0x30, 0x30, 0x33, 0x2E, 0x31, 0x30}};
        FILE_SIGNATURE_MAP.put("FLT", extFLT);
        // FLV ['Flash video file']
        byte[][] extFLV = {{0x46, 0x4C, 0x56}};
        FILE_SIGNATURE_MAP.put("FLV", extFLV);
        // FM ['Adobe FrameMaker']
        byte[][] extFM = {{0x3C, 0x4D, 0x61, 0x6B, 0x65, 0x72, 0x46, 0x69}};
        FILE_SIGNATURE_MAP.put("FM", extFM);
        // FON ['Font file']
        byte[][] extFON = {{0x4D, 0x5A}};
        FILE_SIGNATURE_MAP.put("FON", extFON);
        // FTR ['WinPharoah filter file']
        byte[][] extFTR = {{(byte) 0xD2, 0x0A, 0x00, 0x00}};
        FILE_SIGNATURE_MAP.put("FTR", extFTR);
        // GHO ['Symantex Ghost image file']
        byte[][] extGHO = {{(byte) 0xFE, (byte) 0xEF}};
        FILE_SIGNATURE_MAP.put("GHO", extGHO);
        // GHS ['Symantex Ghost image file']
        byte[][] extGHS = {{(byte) 0xFE, (byte) 0xEF}};
        FILE_SIGNATURE_MAP.put("GHS", extGHS);
        // GID ['Windows Help file_2','Windows help file_3']
        byte[][] extGID = {{0x3F, 0x5F, 0x03, 0x00}, {0x4C, 0x4E, 0x02, 0x00}};
        FILE_SIGNATURE_MAP.put("GID", extGID);
        // GIF ['GIF file']
        byte[][] extGIF = {{0x47, 0x49, 0x46, 0x38}};
        FILE_SIGNATURE_MAP.put("GIF", extGIF);
        // GPG ['GPG public keyring']
        byte[][] extGPG = {{(byte) 0x99}};
        FILE_SIGNATURE_MAP.put("GPG", extGPG);
        // GRP ['Windows Program Manager group file']
        byte[][] extGRP = {{0x50, 0x4D, 0x43, 0x43}};
        FILE_SIGNATURE_MAP.put("GRP", extGRP);
        // GX2 ['Show Partner graphics file']
        byte[][] extGX2 = {{0x47, 0x58, 0x32}};
        FILE_SIGNATURE_MAP.put("GX2", extGX2);
        // GZ ['GZIP archive file']
        byte[][] extGZ = {{0x1F, (byte) 0x8B, 0x08}};
        FILE_SIGNATURE_MAP.put("GZ", extGZ);
        // HAP ['Hamarsoft compressed archive']
        byte[][] extHAP = {{(byte) 0x91, 0x33, 0x48, 0x46}};
        FILE_SIGNATURE_MAP.put("HAP", extHAP);
        // HDMP ['Windows dump file']
        byte[][] extHDMP = {{0x4D, 0x44, 0x4D, 0x50, (byte) 0x93, (byte) 0xA7}};
        FILE_SIGNATURE_MAP.put("HDMP", extHDMP);
        // HDR ['Install Shield compressed file','Radiance High Dynamic Range image file']
        byte[][] extHDR = {{0x49, 0x53, 0x63, 0x28}, {0x23, 0x3F, 0x52, 0x41, 0x44, 0x49, 0x41, 0x4E}};
        FILE_SIGNATURE_MAP.put("HDR", extHDR);
        // hip ['Houdini image file. Three-dimensional modeling and animation']
        byte[][] exthip = {{0x48, 0x69, 0x50, 0x21}};
        FILE_SIGNATURE_MAP.put("hip", exthip);
        // HLP ['Windows Help file_1','Windows Help file_2','Windows help file_3']
        byte[][] extHLP = {{0x00, 0x00, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF}, {0x3F, 0x5F, 0x03, 0x00}, {0x4C, 0x4E, 0x02, 0x00}};
        FILE_SIGNATURE_MAP.put("HLP", extHLP);
        // HQX ['BinHex 4 Compressed Archive']
        byte[][] extHQX = {{0x28, 0x54, 0x68, 0x69, 0x73, 0x20, 0x66, 0x69}};
        FILE_SIGNATURE_MAP.put("HQX", extHQX);
        // ICO ['Windows icon']
        byte[][] extICO = {{0x00, 0x00, 0x01, 0x00}};
        FILE_SIGNATURE_MAP.put("ICO", extICO);
        // IDX ['AOL user configuration','AOL config files','Quicken QuickFinder Information File']
        byte[][] extIDX = {{0x41, 0x4F, 0x4C, 0x44, 0x42}, {0x41, 0x4F, 0x4C}, {0x50, 0x00, 0x00, 0x00, 0x20, 0x00, 0x00, 0x00}};
        FILE_SIGNATURE_MAP.put("IDX", extIDX);
        // IFO ['DVD info file']
        byte[][] extIFO = {{0x44, 0x56, 0x44}};
        FILE_SIGNATURE_MAP.put("IFO", extIFO);
        // IMG ['ChromaGraph Graphics Card Bitmap','GEM Raster file','Img Software Bitmap']
        byte[][] extIMG = {{0x50, 0x49, 0x43, 0x54, 0x00, 0x08}, {(byte) 0xEB, 0x3C, (byte) 0x90, 0x2A}, {0x53, 0x43, 0x4D, 0x49}};
        FILE_SIGNATURE_MAP.put("IMG", extIMG);
        // IND ['AOL client preferences','AOL config files']
        byte[][] extIND = {{0x41, 0x4F, 0x4C, 0x49, 0x44, 0x58}, {0x41, 0x4F, 0x4C}};
        FILE_SIGNATURE_MAP.put("IND", extIND);
        // INFO ['Amiga icon','GNU Info Reader file','ZoomBrowser Image Index']
        byte[][] extINFO = {{(byte) 0xE3, 0x10, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00}, {0x54, 0x68, 0x69, 0x73, 0x20, 0x69, 0x73, 0x20}, {0x7A, 0x62, 0x65, 0x78}};
        FILE_SIGNATURE_MAP.put("INFO", extINFO);
        // ISO ['ISO-9660 CD Disc Image']
        byte[][] extISO = {{0x43, 0x44, 0x30, 0x30, 0x31}};
        FILE_SIGNATURE_MAP.put("ISO", extISO);
        // IVR ['RealPlayer video file (V11+)']
        byte[][] extIVR = {{0x2E, 0x52, 0x45, 0x43}};
        FILE_SIGNATURE_MAP.put("IVR", extIVR);
        // JAR ['Java archive_1','Jar archive','JARCS compressed archive','Java archive_2']
        byte[][] extJAR = {{0x50, 0x4B, 0x03, 0x04}, {0x5F, 0x27, (byte) 0xA8, (byte) 0x89}, {0x4A, 0x41, 0x52, 0x43, 0x53, 0x00}, {0x50, 0x4B, 0x03, 0x04, 0x14, 0x00, 0x08, 0x00}};
        FILE_SIGNATURE_MAP.put("JAR", extJAR);
        // JFIF ['JPEG IMAGE','JFIF IMAGE FILE - jpeg']
        byte[][] extJFIF = {{(byte) 0xFF, (byte) 0xD8, (byte) 0xFF, (byte) 0xE0}, {(byte) 0xFF, (byte) 0xD8, (byte) 0xFF, (byte) 0xE0}};
        FILE_SIGNATURE_MAP.put("JFIF", extJFIF);
        // JG ['AOL ART file_1','AOL ART file_2']
        byte[][] extJG = {{0x4A, 0x47, 0x03, 0x0E}, {0x4A, 0x47, 0x04, 0x0E}};
        FILE_SIGNATURE_MAP.put("JG", extJG);
        // JNT ['MS Windows journal']
        byte[][] extJNT = {{0x4E, 0x42, 0x2A, 0x00}};
        FILE_SIGNATURE_MAP.put("JNT", extJNT);
        // JP2 ['JPEG2000 image files']
        byte[][] extJP2 = {{0x00, 0x00, 0x00, 0x0C, 0x6A, 0x50, 0x20, 0x20}};
        FILE_SIGNATURE_MAP.put("JP2", extJP2);
        // JPE ['JPEG IMAGE','JPE IMAGE FILE - jpeg']
        byte[][] extJPE = {{(byte) 0xFF, (byte) 0xD8, (byte) 0xFF, (byte) 0xE0}, {(byte) 0xFF, (byte) 0xD8, (byte) 0xFF, (byte) 0xE0}};
        FILE_SIGNATURE_MAP.put("JPE", extJPE);
        // JPEG ['JPEG IMAGE','CANNON EOS JPEG FILE','SAMSUNG D500 JPEG FILE']
        byte[][] extJPEG = {{(byte) 0xFF, (byte) 0xD8, (byte) 0xFF, (byte) 0xE0}, {(byte) 0xFF, (byte) 0xD8, (byte) 0xFF, (byte) 0xE2}, {(byte) 0xFF, (byte) 0xD8, (byte) 0xFF, (byte) 0xE3}};
        FILE_SIGNATURE_MAP.put("JPEG", extJPEG);
        // JPG ['JPEG IMAGE','Digital camera JPG using Exchangeable Image File Format (EXIF)','Still Picture Interchange File Format (SPIFF)']
        byte[][] extJPG = {{(byte) 0xFF, (byte) 0xD8, (byte) 0xFF, (byte) 0xE0}, {(byte) 0xFF, (byte) 0xD8, (byte) 0xFF, (byte) 0xE1}, {(byte) 0xFF, (byte) 0xD8, (byte) 0xFF, (byte) 0xE8}};
        FILE_SIGNATURE_MAP.put("JPG", extJPG);
        // JTP ['MS Windows journal']
        byte[][] extJTP = {{0x4E, 0x42, 0x2A, 0x00}};
        FILE_SIGNATURE_MAP.put("JTP", extJTP);
        // KGB ['KGB archive']
        byte[][] extKGB = {{0x4B, 0x47, 0x42, 0x5F, 0x61, 0x72, 0x63, 0x68}};
        FILE_SIGNATURE_MAP.put("KGB", extKGB);
        // KOZ ['Sprint Music Store audio']
        byte[][] extKOZ = {{0x49, 0x44, 0x33, 0x03, 0x00, 0x00, 0x00}};
        FILE_SIGNATURE_MAP.put("KOZ", extKOZ);
        // KWD ['KWord document']
        byte[][] extKWD = {{0x50, 0x4B, 0x03, 0x04}};
        FILE_SIGNATURE_MAP.put("KWD", extKWD);
        // LBK ['Jeppesen FliteLog file']
        byte[][] extLBK = {{(byte) 0xC8, 0x00, 0x79, 0x00}};
        FILE_SIGNATURE_MAP.put("LBK", extLBK);
        // LGC ['Windows application log']
        byte[][] extLGC = {{0x7B, 0x0D, 0x0A, 0x6F, 0x20}};
        FILE_SIGNATURE_MAP.put("LGC", extLGC);
        // LGD ['Windows application log']
        byte[][] extLGD = {{0x7B, 0x0D, 0x0A, 0x6F, 0x20}};
        FILE_SIGNATURE_MAP.put("LGD", extLGD);
        // LHA ['Compressed archive']
        byte[][] extLHA = {{0x2D, 0x6C, 0x68}};
        FILE_SIGNATURE_MAP.put("LHA", extLHA);
        // LIB ['Unix archiver (ar)']
        byte[][] extLIB = {{0x21, 0x3C, 0x61, 0x72, 0x63, 0x68, 0x3E, 0x0A}};
        FILE_SIGNATURE_MAP.put("LIB", extLIB);
        // LIT ['MS Reader eBook']
        byte[][] extLIT = {{0x49, 0x54, 0x4F, 0x4C, 0x49, 0x54, 0x4C, 0x53}};
        FILE_SIGNATURE_MAP.put("LIT", extLIT);
        // LNK ['Windows shortcut file']
        byte[][] extLNK = {{0x4C, 0x00, 0x00, 0x00, 0x01, 0x14, 0x02, 0x00}};
        FILE_SIGNATURE_MAP.put("LNK", extLNK);
        // LOG ['Symantec Wise Installer log']
        byte[][] extLOG = {{0x2A, 0x2A, 0x2A, 0x20, 0x20, 0x49, 0x6E, 0x73}};
        FILE_SIGNATURE_MAP.put("LOG", extLOG);
        // LWP ['Lotus WordPro file']
        byte[][] extLWP = {{0x57, 0x6F, 0x72, 0x64, 0x50, 0x72, 0x6F}};
        FILE_SIGNATURE_MAP.put("LWP", extLWP);
        // LZH ['Compressed archive']
        byte[][] extLZH = {{0x2D, 0x6C, 0x68}};
        FILE_SIGNATURE_MAP.put("LZH", extLZH);
        // M4A ['Apple audio and video files']
        byte[][] extM4A = {{0x00, 0x00, 0x00, 0x20, 0x66, 0x74, 0x79, 0x70, 0x4D, 0x34, 0x41}};
        FILE_SIGNATURE_MAP.put("M4A", extM4A);
        // MANIFEST ['Windows Visual Stylesheet']
        byte[][] extMANIFEST = {{0x3C, 0x3F, 0x78, 0x6D, 0x6C, 0x20, 0x76, 0x65, 0x72, 0x73, 0x69, 0x6F, 0x6E, 0x3D}};
        FILE_SIGNATURE_MAP.put("MANIFEST", extMANIFEST);
        // MAR ['MAr compressed archive','Microsoft','Mozilla archive']
        byte[][] extMAR = {{0x4D, 0x41, 0x72, 0x30, 0x00}, {0x4D, 0x41, 0x52, 0x43}, {0x4D, 0x41, 0x52, 0x31, 0x00}};
        FILE_SIGNATURE_MAP.put("MAR", extMAR);
        // MDB ['Microsoft Access']
        byte[][] extMDB = {{0x00, 0x01, 0x00, 0x00, 0x53, 0x74, 0x61, 0x6E, 0x64, 0x61, 0x72, 0x64, 0x20, 0x4A, 0x65, 0x74, 0x20, 0x44, 0x42}};
        FILE_SIGNATURE_MAP.put("MDB", extMDB);
        // MDF ['SQL Data Base']
        byte[][] extMDF = {{0x01, 0x0F, 0x00, 0x00}};
        FILE_SIGNATURE_MAP.put("MDF", extMDF);
        // MDI ['MS Document Imaging file']
        byte[][] extMDI = {{0x45, 0x50}};
        FILE_SIGNATURE_MAP.put("MDI", extMDI);
        // MID ['MIDI sound file']
        byte[][] extMID = {{0x4D, 0x54, 0x68, 0x64}};
        FILE_SIGNATURE_MAP.put("MID", extMID);
        // MIDI ['MIDI sound file']
        byte[][] extMIDI = {{0x4D, 0x54, 0x68, 0x64}};
        FILE_SIGNATURE_MAP.put("MIDI", extMIDI);
        // MIF ['Adobe FrameMaker','MapInfo Interchange Format file']
        byte[][] extMIF = {{0x3C, 0x4D, 0x61, 0x6B, 0x65, 0x72, 0x46, 0x69}, {0x56, 0x65, 0x72, 0x73, 0x69, 0x6F, 0x6E, 0x20}};
        FILE_SIGNATURE_MAP.put("MIF", extMIF);
        // MKV ['Matroska stream file']
        byte[][] extMKV = {{0x1A, 0x45, (byte) 0xDF, (byte) 0xA3, (byte) 0x93, 0x42, (byte) 0x82, (byte) 0x88}};
        FILE_SIGNATURE_MAP.put("MKV", extMKV);
        // MLS ['Milestones project management file','Milestones project management file_1','Milestones project management file_2','Skype localization data file']
        byte[][] extMLS = {{0x4D, 0x49, 0x4C, 0x45, 0x53}, {0x4D, 0x56, 0x32, 0x31, 0x34}, {0x4D, 0x56, 0x32, 0x43}, {0x4D, 0x4C, 0x53, 0x57}};
        FILE_SIGNATURE_MAP.put("MLS", extMLS);
        // MMF ['Yamaha Synthetic music Mobile Application Format']
        byte[][] extMMF = {{0x4D, 0x4D, 0x4D, 0x44, 0x00, 0x00}};
        FILE_SIGNATURE_MAP.put("MMF", extMMF);
        // MNY ['Microsoft Money file']
        byte[][] extMNY = {{0x00, 0x01, 0x00, 0x00, 0x4D, 0x53, 0x49, 0x53, 0x41, 0x4D, 0x20, 0x44, 0x61, 0x74, 0x61, 0x62, 0x61, 0x73, 0x65}};
        FILE_SIGNATURE_MAP.put("MNY", extMNY);
        // MOF ['MSinfo file']
        byte[][] extMOF = {{(byte) 0xFF, (byte) 0xFE, 0x23, 0x00, 0x6C, 0x00, 0x69, 0x00}};
        FILE_SIGNATURE_MAP.put("MOF", extMOF);
        // MOV ['QuickTime movie_1','QuickTime movie_2','QuickTime movie_3','QuickTime movie_4','QuickTime movie_5','QuickTime movie_6']
        byte[][] extMOV = {{0x6D, 0x6F, 0x6F, 0x76}, {0x66, 0x72, 0x65, 0x65}, {0x6D, 0x64, 0x61, 0x74}, {0x77, 0x69, 0x64, 0x65}, {0x70, 0x6E, 0x6F, 0x74}, {0x73, 0x6B, 0x69, 0x70}};
        FILE_SIGNATURE_MAP.put("MOV", extMOV);
        // MP ['Monochrome Picture TIFF bitmap']
        byte[][] extMP = {{0x0C, (byte) 0xED}};
        FILE_SIGNATURE_MAP.put("MP", extMP);
        // MP3 ['MP3 audio file']
        byte[][] extMP3 = {{0x49, 0x44, 0x33}};
        FILE_SIGNATURE_MAP.put("MP3", extMP3);
        // MPG ['DVD video file','MPEG video file']
        byte[][] extMPG = {{0x00, 0x00, 0x01, (byte) 0xBA}, {0x00, 0x00, 0x01, (byte) 0xB3}};
        FILE_SIGNATURE_MAP.put("MPG", extMPG);
        // MSC ['Microsoft Common Console Document','MMC Snap-in Control file']
        byte[][] extMSC = {{(byte) 0xD0, (byte) 0xCF, 0x11, (byte) 0xE0, (byte) 0xA1, (byte) 0xB1, 0x1A, (byte) 0xE1}, {0x3C, 0x3F, 0x78, 0x6D, 0x6C, 0x20, 0x76, 0x65, 0x72, 0x73, 0x69, 0x6F, 0x6E, 0x3D, 0x22, 0x31, 0x2E, 0x30, 0x22, 0x3F, 0x3E, 0x0D, 0x0A, 0x3C, 0x4D, 0x4D, 0x43, 0x5F, 0x43, 0x6F, 0x6E, 0x73, 0x6F, 0x6C, 0x65, 0x46, 0x69, 0x6C, 0x65, 0x20, 0x43, 0x6F, 0x6E, 0x73, 0x6F, 0x6C, 0x65, 0x56, 0x65, 0x72}};
        FILE_SIGNATURE_MAP.put("MSC", extMSC);
        // MSI ['Microsoft Installer package','Cerius2 file']
        byte[][] extMSI = {{(byte) 0xD0, (byte) 0xCF, 0x11, (byte) 0xE0, (byte) 0xA1, (byte) 0xB1, 0x1A, (byte) 0xE1}, {0x23, 0x20}};
        FILE_SIGNATURE_MAP.put("MSI", extMSI);
        // MSV ['Sony Compressed Voice File']
        byte[][] extMSV = {{0x4D, 0x53, 0x5F, 0x56, 0x4F, 0x49, 0x43, 0x45}};
        FILE_SIGNATURE_MAP.put("MSV", extMSV);
        // MTW ['Minitab data file']
        byte[][] extMTW = {{(byte) 0xD0, (byte) 0xCF, 0x11, (byte) 0xE0, (byte) 0xA1, (byte) 0xB1, 0x1A, (byte) 0xE1}};
        FILE_SIGNATURE_MAP.put("MTW", extMTW);
        // NRI ['Nero CD compilation']
        byte[][] extNRI = {{0x0E, 0x4E, 0x65, 0x72, 0x6F, 0x49, 0x53, 0x4F}};
        FILE_SIGNATURE_MAP.put("NRI", extNRI);
        // NSF ['Lotus Notes database','NES Sound file']
        byte[][] extNSF = {{0x1A, 0x00, 0x00, 0x04, 0x00, 0x00}, {0x4E, 0x45, 0x53, 0x4D, 0x1A, 0x01}};
        FILE_SIGNATURE_MAP.put("NSF", extNSF);
        // NTF ['Lotus Notes database template','National Imagery Transmission Format file','National Transfer Format Map']
        byte[][] extNTF = {{0x1A, 0x00, 0x00}, {0x4E, 0x49, 0x54, 0x46, 0x30}, {0x30, 0x31, 0x4F, 0x52, 0x44, 0x4E, 0x41, 0x4E}};
        FILE_SIGNATURE_MAP.put("NTF", extNTF);
        // NVRAM ['VMware BIOS state file']
        byte[][] extNVRAM = {{0x4D, 0x52, 0x56, 0x4E}};
        FILE_SIGNATURE_MAP.put("NVRAM", extNVRAM);
        // OBJ ['MS COFF relocatable object code','Relocatable object code']
        byte[][] extOBJ = {{0x4C, 0x01}, {(byte) 0x80}};
        FILE_SIGNATURE_MAP.put("OBJ", extOBJ);
        // OCX ['ActiveX']
        byte[][] extOCX = {{0x4D, 0x5A}};
        FILE_SIGNATURE_MAP.put("OCX", extOCX);
        // ODP ['OpenDocument template']
        byte[][] extODP = {{0x50, 0x4B, 0x03, 0x04}};
        FILE_SIGNATURE_MAP.put("ODP", extODP);
        // ODT ['OpenDocument template']
        byte[][] extODT = {{0x50, 0x4B, 0x03, 0x04}};
        FILE_SIGNATURE_MAP.put("ODT", extODT);
        // OGA ['Ogg Vorbis Codec compressed file']
        byte[][] extOGA = {{0x4F, 0x67, 0x67, 0x53, 0x00, 0x02, 0x00, 0x00}};
        FILE_SIGNATURE_MAP.put("OGA", extOGA);
        // OGG ['Ogg Vorbis Codec compressed file']
        byte[][] extOGG = {{0x4F, 0x67, 0x67, 0x53, 0x00, 0x02, 0x00, 0x00}};
        FILE_SIGNATURE_MAP.put("OGG", extOGG);
        // OGV ['Ogg Vorbis Codec compressed file']
        byte[][] extOGV = {{0x4F, 0x67, 0x67, 0x53, 0x00, 0x02, 0x00, 0x00}};
        FILE_SIGNATURE_MAP.put("OGV", extOGV);
        // OGX ['Ogg Vorbis Codec compressed file']
        byte[][] extOGX = {{0x4F, 0x67, 0x67, 0x53, 0x00, 0x02, 0x00, 0x00}};
        FILE_SIGNATURE_MAP.put("OGX", extOGX);
        // OLB ['OLE object library']
        byte[][] extOLB = {{0x4D, 0x5A}};
        FILE_SIGNATURE_MAP.put("OLB", extOLB);
        // ONE ['MS OneNote note']
        byte[][] extONE = {{(byte) 0xE4, 0x52, 0x5C, 0x7B, (byte) 0x8C, (byte) 0xD8, (byte) 0xA7, 0x4D}};
        FILE_SIGNATURE_MAP.put("ONE", extONE);
        // OPT ['Developer Studio File Options file','Developer Studio subheader']
        byte[][] extOPT = {{(byte) 0xD0, (byte) 0xCF, 0x11, (byte) 0xE0, (byte) 0xA1, (byte) 0xB1, 0x1A, (byte) 0xE1}, {(byte) 0xFD, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF, 0x20}};
        FILE_SIGNATURE_MAP.put("OPT", extOPT);
        // ORG ['AOL personal file cabinet']
        byte[][] extORG = {{0x41, 0x4F, 0x4C, 0x56, 0x4D, 0x31, 0x30, 0x30}};
        FILE_SIGNATURE_MAP.put("ORG", extORG);
        // OTT ['OpenDocument template']
        byte[][] extOTT = {{0x50, 0x4B, 0x03, 0x04}};
        FILE_SIGNATURE_MAP.put("OTT", extOTT);
        // P10 ['Intel PROset']
        byte[][] extP10 = {{0x64, 0x00, 0x00, 0x00}};
        FILE_SIGNATURE_MAP.put("P10", extP10);
        // PAK ['PAK Compressed archive file','Quake archive file']
        byte[][] extPAK = {{0x1A, 0x0B}, {0x50, 0x41, 0x43, 0x4B}};
        FILE_SIGNATURE_MAP.put("PAK", extPAK);
        // PAT ['GIMP pattern file']
        byte[][] extPAT = {{0x47, 0x50, 0x41, 0x54}};
        FILE_SIGNATURE_MAP.put("PAT", extPAT);
        // PAX ['PAX password protected bitmap']
        byte[][] extPAX = {{0x50, 0x41, 0x58}};
        FILE_SIGNATURE_MAP.put("PAX", extPAX);
        // PCH ['Visual C PreCompiled header']
        byte[][] extPCH = {{0x56, 0x43, 0x50, 0x43, 0x48, 0x30}};
        FILE_SIGNATURE_MAP.put("PCH", extPCH);
        // PCX ['ZSOFT Paintbrush file_1','ZSOFT Paintbrush file_2','ZSOFT Paintbrush file_3']
        byte[][] extPCX = {{0x0A, 0x02, 0x01, 0x01}, {0x0A, 0x03, 0x01, 0x01}, {0x0A, 0x05, 0x01, 0x01}};
        FILE_SIGNATURE_MAP.put("PCX", extPCX);
        // PDB ['BGBlitz position database file','Merriam-Webster Pocket Dictionary','MS C++ debugging symbols file','PalmOS SuperMemo','PowerBASIC Debugger Symbols']
        byte[][] extPDB = {{(byte) 0xAC, (byte) 0xED, 0x00, 0x05, 0x73, 0x72, 0x00, 0x12}, {0x4D, 0x2D, 0x57, 0x20, 0x50, 0x6F, 0x63, 0x6B}, {0x4D, 0x69, 0x63, 0x72, 0x6F, 0x73, 0x6F, 0x66, 0x74, 0x20, 0x43, 0x2F, 0x43, 0x2B, 0x2B, 0x20}, {0x73, 0x6D, 0x5F}, {0x73, 0x7A, 0x65, 0x7A}};
        FILE_SIGNATURE_MAP.put("PDB", extPDB);
        // PDF ['PDF file']
        byte[][] extPDF = {{0x25, 0x50, 0x44, 0x46}};
        FILE_SIGNATURE_MAP.put("PDF", extPDF);
        // PF ['Windows prefetch file']
        byte[][] extPF = {{0x11, 0x00, 0x00, 0x00, 0x53, 0x43, 0x43, 0x41}};
        FILE_SIGNATURE_MAP.put("PF", extPF);
        // PFC ['AOL config files','AOL personal file cabinet']
        byte[][] extPFC = {{0x41, 0x4F, 0x4C}, {0x41, 0x4F, 0x4C, 0x56, 0x4D, 0x31, 0x30, 0x30}};
        FILE_SIGNATURE_MAP.put("PFC", extPFC);
        // PGD ['PGP disk image']
        byte[][] extPGD = {{0x50, 0x47, 0x50, 0x64, 0x4D, 0x41, 0x49, 0x4E}};
        FILE_SIGNATURE_MAP.put("PGD", extPGD);
        // PGM ['Portable Graymap Graphic']
        byte[][] extPGM = {{0x50, 0x35, 0x0A}};
        FILE_SIGNATURE_MAP.put("PGM", extPGM);
        // PIF ['Windows']
        byte[][] extPIF = {{0x4D, 0x5A}};
        FILE_SIGNATURE_MAP.put("PIF", extPIF);
        // PKR ['PGP public keyring']
        byte[][] extPKR = {{(byte) 0x99, 0x01}};
        FILE_SIGNATURE_MAP.put("PKR", extPKR);
        // PNG ['PNG image']
        byte[][] extPNG = {{(byte) 0x89, 0x50, 0x4E, 0x47, 0x0D, 0x0A, 0x1A, 0x0A}};
        FILE_SIGNATURE_MAP.put("PNG", extPNG);
        // PPS ['Microsoft Office document']
        byte[][] extPPS = {{(byte) 0xD0, (byte) 0xCF, 0x11, (byte) 0xE0, (byte) 0xA1, (byte) 0xB1, 0x1A, (byte) 0xE1}};
        FILE_SIGNATURE_MAP.put("PPS", extPPS);
        // PPT ['Microsoft Office document','PowerPoint presentation subheader_1','PowerPoint presentation subheader_2','PowerPoint presentation subheader_3','PowerPoint presentation subheader_4','PowerPoint presentation subheader_5','PowerPoint presentation subheader_6']
        byte[][] extPPT = {{(byte) 0xD0, (byte) 0xCF, 0x11, (byte) 0xE0, (byte) 0xA1, (byte) 0xB1, 0x1A, (byte) 0xE1}, {0x00, 0x6E, 0x1E, (byte) 0xF0}, {0x0F, 0x00, (byte) 0xE8, 0x03}, {(byte) 0xA0, 0x46, 0x1D, (byte) 0xF0}, {(byte) 0xFD, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF, 0x0E, 0x00, 0x00, 0x00}, {(byte) 0xFD, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF, 0x1C, 0x00, 0x00, 0x00}, {(byte) 0xFD, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF, 0x43, 0x00, 0x00, 0x00}};
        FILE_SIGNATURE_MAP.put("PPT", extPPT);
        // PPTX ['MS Office Open XML Format Document','MS Office 2007 documents']
        byte[][] extPPTX = {{0x50, 0x4B, 0x03, 0x04}, {0x50, 0x4B, 0x03, 0x04, 0x14, 0x00, 0x06, 0x00}};
        FILE_SIGNATURE_MAP.put("PPTX", extPPTX);
        // PPZ ['Powerpoint Packaged Presentation']
        byte[][] extPPZ = {{0x4D, 0x53, 0x43, 0x46}};
        FILE_SIGNATURE_MAP.put("PPZ", extPPZ);
        // PRC ['Palmpilot resource file','PathWay Map file']
        byte[][] extPRC = {{0x42, 0x4F, 0x4F, 0x4B, 0x4D, 0x4F, 0x42, 0x49}, {0x74, 0x42, 0x4D, 0x50, 0x4B, 0x6E, 0x57, 0x72}};
        FILE_SIGNATURE_MAP.put("PRC", extPRC);
        // PSD ['Photoshop image']
        byte[][] extPSD = {{0x38, 0x42, 0x50, 0x53}};
        FILE_SIGNATURE_MAP.put("PSD", extPSD);
        // PSP ['Corel Paint Shop Pro image']
        byte[][] extPSP = {{0x7E, 0x42, 0x4B, 0x00}};
        FILE_SIGNATURE_MAP.put("PSP", extPSP);
        // PUB ['MS Publisher file']
        byte[][] extPUB = {{(byte) 0xD0, (byte) 0xCF, 0x11, (byte) 0xE0, (byte) 0xA1, (byte) 0xB1, 0x1A, (byte) 0xE1}};
        FILE_SIGNATURE_MAP.put("PUB", extPUB);
        // PWI ['MS WinMobile personal note']
        byte[][] extPWI = {{0x7B, 0x5C, 0x70, 0x77, 0x69}};
        FILE_SIGNATURE_MAP.put("PWI", extPWI);
        // PWL ['Win95 password file','Win98 password file']
        byte[][] extPWL = {{(byte) 0xB0, 0x4D, 0x46, 0x43}, {(byte) 0xE3, (byte) 0x82, (byte) 0x85, (byte) 0x96}};
        FILE_SIGNATURE_MAP.put("PWL", extPWL);
        // QBB ['QuickBooks backup']
        byte[][] extQBB = {{0x45, (byte) 0x86, 0x00, 0x00, 0x06, 0x00}};
        FILE_SIGNATURE_MAP.put("QBB", extQBB);
        // QCP ['Resource Interchange File Format']
        byte[][] extQCP = {{0x52, 0x49, 0x46, 0x46}};
        FILE_SIGNATURE_MAP.put("QCP", extQCP);
        // QDF ['QDF Quicken data']
        byte[][] extQDF = {{(byte) 0xAC, (byte) 0x9E, (byte) 0xBD, (byte) 0x8F, 0x00, 0x00}};
        FILE_SIGNATURE_MAP.put("QDF", extQDF);
        // QEL ['QDL Quicken data']
        byte[][] extQEL = {{0x51, 0x45, 0x4C, 0x20}};
        FILE_SIGNATURE_MAP.put("QEL", extQEL);
        // QEMU ['Qcow Disk Image']
        byte[][] extQEMU = {{0x51, 0x46, 0x49}};
        FILE_SIGNATURE_MAP.put("QEMU", extQEMU);
        // QPH ['Quicken price history']
        byte[][] extQPH = {{0x03, 0x00, 0x00, 0x00}};
        FILE_SIGNATURE_MAP.put("QPH", extQPH);
        // QSD ['ABD']
        byte[][] extQSD = {{0x51, 0x57, 0x20, 0x56, 0x65, 0x72, 0x2E, 0x20}};
        FILE_SIGNATURE_MAP.put("QSD", extQSD);
        // QTS ['Windows']
        byte[][] extQTS = {{0x4D, 0x5A}};
        FILE_SIGNATURE_MAP.put("QTS", extQTS);
        // QTX ['Windows']
        byte[][] extQTX = {{0x4D, 0x5A}};
        FILE_SIGNATURE_MAP.put("QTX", extQTX);
        // QXD ['Quark Express (Intel)','Quark Express (Motorola)']
        byte[][] extQXD = {{0x00, 0x00, 0x49, 0x49, 0x58, 0x50, 0x52}, {0x00, 0x00, 0x4D, 0x4D, 0x58, 0x50, 0x52}};
        FILE_SIGNATURE_MAP.put("QXD", extQXD);
        // RA ['RealAudio file','RealAudio streaming media']
        byte[][] extRA = {{0x2E, 0x52, 0x4D, 0x46, 0x00, 0x00, 0x00, 0x12}, {0x2E, 0x72, 0x61, (byte) 0xFD, 0x00}};
        FILE_SIGNATURE_MAP.put("RA", extRA);
        // RAM ['RealMedia metafile']
        byte[][] extRAM = {{0x72, 0x74, 0x73, 0x70, 0x3A, 0x2F, 0x2F}};
        FILE_SIGNATURE_MAP.put("RAM", extRAM);
        // RAR ['WinRAR compressed archive']
        byte[][] extRAR = {{0x52, 0x61, 0x72, 0x21, 0x1A, 0x07, 0x00}};
        FILE_SIGNATURE_MAP.put("RAR", extRAR);
        // REG ['Windows Registry file','WinNT Registry']
        byte[][] extREG = {{(byte) 0xFF, (byte) 0xFE}, {0x52, 0x45, 0x47, 0x45, 0x44, 0x49, 0x54}};
        FILE_SIGNATURE_MAP.put("REG", extREG);
        // RGB ['Silicon Graphics RGB Bitmap']
        byte[][] extRGB = {{0x01, (byte) 0xDA, 0x01, 0x01, 0x00, 0x03}};
        FILE_SIGNATURE_MAP.put("RGB", extRGB);
        // RM ['RealMedia streaming media']
        byte[][] extRM = {{0x2E, 0x52, 0x4D, 0x46}};
        FILE_SIGNATURE_MAP.put("RM", extRM);
        // RMI ['Resource Interchange File Format']
        byte[][] extRMI = {{0x52, 0x49, 0x46, 0x46}};
        FILE_SIGNATURE_MAP.put("RMI", extRMI);
        // RMVB ['RealMedia streaming media']
        byte[][] extRMVB = {{0x2E, 0x52, 0x4D, 0x46}};
        FILE_SIGNATURE_MAP.put("RMVB", extRMVB);
        // RPM ['RedHat Package Manager']
        byte[][] extRPM = {{(byte) 0xED, (byte) 0xAB, (byte) 0xEE, (byte) 0xDB}};
        FILE_SIGNATURE_MAP.put("RPM", extRPM);
        // RTD ['RagTime document']
        byte[][] extRTD = {{0x43, 0x23, 0x2B, 0x44, (byte) 0xA4, 0x43, 0x4D, (byte) 0xA5}};
        FILE_SIGNATURE_MAP.put("RTD", extRTD);
        // RTF ['RTF file']
        byte[][] extRTF = {{0x7B, 0x5C, 0x72, 0x74, 0x66, 0x31}};
        FILE_SIGNATURE_MAP.put("RTF", extRTF);
        // RVT ['Revit Project file']
        byte[][] extRVT = {{(byte) 0xD0, (byte) 0xCF, 0x11, (byte) 0xE0, (byte) 0xA1, (byte) 0xB1, 0x1A, (byte) 0xE1}};
        FILE_SIGNATURE_MAP.put("RVT", extRVT);
        // SAM ['Lotus AMI Pro document_1','Lotus AMI Pro document_2']
        byte[][] extSAM = {{0x5B, 0x56, 0x45, 0x52, 0x5D}, {0x5B, 0x76, 0x65, 0x72, 0x5D}};
        FILE_SIGNATURE_MAP.put("SAM", extSAM);
        // SAV ['SPSS Data file']
        byte[][] extSAV = {{0x24, 0x46, 0x4C, 0x32, 0x40, 0x28, 0x23, 0x29}};
        FILE_SIGNATURE_MAP.put("SAV", extSAV);
        // SCR ['Screen saver']
        byte[][] extSCR = {{0x4D, 0x5A}};
        FILE_SIGNATURE_MAP.put("SCR", extSCR);
        // SDR ['SmartDraw Drawing file']
        byte[][] extSDR = {{0x53, 0x4D, 0x41, 0x52, 0x54, 0x44, 0x52, 0x57}};
        FILE_SIGNATURE_MAP.put("SDR", extSDR);
        // SH3 ['Harvard Graphics presentation file']
        byte[][] extSH3 = {{0x48, 0x48, 0x47, 0x42, 0x31}};
        FILE_SIGNATURE_MAP.put("SH3", extSH3);
        // SHD ['Win Server 2003 printer spool file','Win9x printer spool file','Win2000','WinNT printer spool file']
        byte[][] extSHD = {{0x68, 0x49, 0x00, 0x00}, {0x4B, 0x49, 0x00, 0x00}, {0x67, 0x49, 0x00, 0x00}, {0x66, 0x49, 0x00, 0x00}};
        FILE_SIGNATURE_MAP.put("SHD", extSHD);
        // SHW ['Harvard Graphics presentation']
        byte[][] extSHW = {{0x53, 0x48, 0x4F, 0x57}};
        FILE_SIGNATURE_MAP.put("SHW", extSHW);
        // SIT ['StuffIt archive','StuffIt compressed archive']
        byte[][] extSIT = {{0x53, 0x49, 0x54, 0x21, 0x00}, {0x53, 0x74, 0x75, 0x66, 0x66, 0x49, 0x74, 0x20}};
        FILE_SIGNATURE_MAP.put("SIT", extSIT);
        // SKF ['SkinCrafter skin']
        byte[][] extSKF = {{0x07, 0x53, 0x4B, 0x46}};
        FILE_SIGNATURE_MAP.put("SKF", extSKF);
        // SKR ['PGP secret keyring_1','PGP secret keyring_2']
        byte[][] extSKR = {{(byte) 0x95, 0x00}, {(byte) 0x95, 0x01}};
        FILE_SIGNATURE_MAP.put("SKR", extSKR);
        // SLE ['Steganos virtual secure drive','Surfplan kite project file']
        byte[][] extSLE = {{0x41, 0x43, 0x76}, {0x3A, 0x56, 0x45, 0x52, 0x53, 0x49, 0x4F, 0x4E}};
        FILE_SIGNATURE_MAP.put("SLE", extSLE);
        // SLN ['Visual Studio .NET file']
        byte[][] extSLN = {{0x4D, 0x69, 0x63, 0x72, 0x6F, 0x73, 0x6F, 0x66, 0x74, 0x20, 0x56, 0x69, 0x73, 0x75, 0x61, 0x6C}};
        FILE_SIGNATURE_MAP.put("SLN", extSLN);
        // SNM ['Netscape Communicator (v4) mail folder']
        byte[][] extSNM = {{0x00, 0x1E, (byte) 0x84, (byte) 0x90, 0x00, 0x00, 0x00, 0x00}};
        FILE_SIGNATURE_MAP.put("SNM", extSNM);
        // SNP ['MS Access Snapshot Viewer file']
        byte[][] extSNP = {{0x4D, 0x53, 0x43, 0x46}};
        FILE_SIGNATURE_MAP.put("SNP", extSNP);
        // SOU ['Visual Studio Solution User Options file']
        byte[][] extSOU = {{(byte) 0xD0, (byte) 0xCF, 0x11, (byte) 0xE0, (byte) 0xA1, (byte) 0xB1, 0x1A, (byte) 0xE1}};
        FILE_SIGNATURE_MAP.put("SOU", extSOU);
        // SPL ['Windows icon']
        byte[][] extSPL = {{0x00, 0x00, 0x01, 0x00}};
        FILE_SIGNATURE_MAP.put("SPL", extSPL);
        // SPO ['SPSS output file']
        byte[][] extSPO = {{(byte) 0xD0, (byte) 0xCF, 0x11, (byte) 0xE0, (byte) 0xA1, (byte) 0xB1, 0x1A, (byte) 0xE1}};
        FILE_SIGNATURE_MAP.put("SPO", extSPO);
        // SUD ['WinNT Registry']
        byte[][] extSUD = {{0x52, 0x45, 0x47, 0x45, 0x44, 0x49, 0x54}};
        FILE_SIGNATURE_MAP.put("SUD", extSUD);
        // SUO ['Visual Studio Solution subheader']
        byte[][] extSUO = {{(byte) 0xFD, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF, 0x04}};
        FILE_SIGNATURE_MAP.put("SUO", extSUO);
        // SWF ['Shockwave Flash file','Shockwave Flash player']
        byte[][] extSWF = {{0x43, 0x57, 0x53}, {0x46, 0x57, 0x53}};
        FILE_SIGNATURE_MAP.put("SWF", extSWF);
        // SXC ['StarOffice spreadsheet']
        byte[][] extSXC = {{0x50, 0x4B, 0x03, 0x04}};
        FILE_SIGNATURE_MAP.put("SXC", extSXC);
        // SXD ['OpenOffice documents']
        byte[][] extSXD = {{0x50, 0x4B, 0x03, 0x04}};
        FILE_SIGNATURE_MAP.put("SXD", extSXD);
        // SXI ['OpenOffice documents']
        byte[][] extSXI = {{0x50, 0x4B, 0x03, 0x04}};
        FILE_SIGNATURE_MAP.put("SXI", extSXI);
        // SXW ['OpenOffice documents']
        byte[][] extSXW = {{0x50, 0x4B, 0x03, 0x04}};
        FILE_SIGNATURE_MAP.put("SXW", extSXW);
        // SYS ['Windows','DOS system driver','Keyboard driver file','Windows executable file_1','Windows executable file_2','Windows executable file_3','Windows executable']
        byte[][] extSYS = {{0x4D, 0x5A}, {(byte) 0xFF, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF}, {(byte) 0xFF, 0x4B, 0x45, 0x59, 0x42, 0x20, 0x20, 0x20}, {(byte) 0xE8}, {(byte) 0xE9}, {(byte) 0xEB}, {(byte) 0xFF}};
        FILE_SIGNATURE_MAP.put("SYS", extSYS);
        // SYW ['Harvard Graphics symbol graphic']
        byte[][] extSYW = {{0x41, 0x4D, 0x59, 0x4F}};
        FILE_SIGNATURE_MAP.put("SYW", extSYW);
        // TAR ['Tape Archive']
        byte[][] extTAR = {{0x75, 0x73, 0x74, 0x61, 0x72}};
        FILE_SIGNATURE_MAP.put("TAR", extTAR);
        // TAR.BZ2 ['bzip2 compressed archive']
        byte[][] extTARpointBZ2 = {{0x42, 0x5A, 0x68}};
        FILE_SIGNATURE_MAP.put("TAR.BZ2", extTARpointBZ2);
        // TAR.Z ['Compressed tape archive_1','Compressed tape archive_2']
        byte[][] extTARpointZ = {{0x1F, (byte) 0x9D, (byte) 0x90}, {0x1F, (byte) 0xA0}};
        FILE_SIGNATURE_MAP.put("TAR.Z", extTARpointZ);
        // TB2 ['bzip2 compressed archive']
        byte[][] extTB2 = {{0x42, 0x5A, 0x68}};
        FILE_SIGNATURE_MAP.put("TB2", extTB2);
        // TBZ2 ['bzip2 compressed archive']
        byte[][] extTBZ2 = {{0x42, 0x5A, 0x68}};
        FILE_SIGNATURE_MAP.put("TBZ2", extTBZ2);
        // TIB ['Acronis True Image']
        byte[][] extTIB = {{(byte) 0xB4, 0x6E, 0x68, 0x44}};
        FILE_SIGNATURE_MAP.put("TIB", extTIB);
        // TIF ['TIFF file_1','TIFF file_2','TIFF file_3','TIFF file_4']
        byte[][] extTIF = {{0x49, 0x20, 0x49}, {0x49, 0x49, 0x2A, 0x00}, {0x4D, 0x4D, 0x00, 0x2A}, {0x4D, 0x4D, 0x00, 0x2B}};
        FILE_SIGNATURE_MAP.put("TIF", extTIF);
        // TIFF ['TIFF file_1','TIFF file_2','TIFF file_3','TIFF file_4']
        byte[][] extTIFF = {{0x49, 0x20, 0x49}, {0x49, 0x49, 0x2A, 0x00}, {0x4D, 0x4D, 0x00, 0x2A}, {0x4D, 0x4D, 0x00, 0x2B}};
        FILE_SIGNATURE_MAP.put("TIFF", extTIFF);
        // TLB ['OLE']
        byte[][] extTLB = {{0x4D, 0x53, 0x46, 0x54, 0x02, 0x00, 0x01, 0x00}};
        FILE_SIGNATURE_MAP.put("TLB", extTLB);
        // TR1 ['Novell LANalyzer capture file']
        byte[][] extTR1 = {{0x01, 0x10}};
        FILE_SIGNATURE_MAP.put("TR1", extTR1);
        // UCE ['Unicode extensions']
        byte[][] extUCE = {{0x55, 0x43, 0x45, 0x58}};
        FILE_SIGNATURE_MAP.put("UCE", extUCE);
        // UFA ['UFA compressed archive']
        byte[][] extUFA = {{0x55, 0x46, 0x41, (byte) 0xC6, (byte) 0xD2, (byte) 0xC1}};
        FILE_SIGNATURE_MAP.put("UFA", extUFA);
        // VBX ['VisualBASIC application']
        byte[][] extVBX = {{0x4D, 0x5A}};
        FILE_SIGNATURE_MAP.put("VBX", extVBX);
        // VCD ['VideoVCD']
        byte[][] extVCD = {{0x45, 0x4E, 0x54, 0x52, 0x59, 0x56, 0x43, 0x44}};
        FILE_SIGNATURE_MAP.put("VCD", extVCD);
        // VCF ['vCard']
        byte[][] extVCF = {{0x42, 0x45, 0x47, 0x49, 0x4E, 0x3A, 0x56, 0x43}};
        FILE_SIGNATURE_MAP.put("VCF", extVCF);
        // VCW ['Visual C++ Workbench Info File']
        byte[][] extVCW = {{0x5B, 0x4D, 0x53, 0x56, 0x43}};
        FILE_SIGNATURE_MAP.put("VCW", extVCW);
        // VHD ['Virtual PC HD image']
        byte[][] extVHD = {{0x63, 0x6F, 0x6E, 0x65, 0x63, 0x74, 0x69, 0x78}};
        FILE_SIGNATURE_MAP.put("VHD", extVHD);
        // VMDK ['VMware 3 Virtual Disk','VMware 4 Virtual Disk description','VMware 4 Virtual Disk']
        byte[][] extVMDK = {{0x43, 0x4F, 0x57, 0x44}, {0x23, 0x20, 0x44, 0x69, 0x73, 0x6B, 0x20, 0x44}, {0x4B, 0x44, 0x4D}};
        FILE_SIGNATURE_MAP.put("VMDK", extVMDK);
        // VOB ['DVD video file']
        byte[][] extVOB = {{0x00, 0x00, 0x01, (byte) 0xBA}};
        FILE_SIGNATURE_MAP.put("VOB", extVOB);
        // VSD ['Visio file']
        byte[][] extVSD = {{(byte) 0xD0, (byte) 0xCF, 0x11, (byte) 0xE0, (byte) 0xA1, (byte) 0xB1, 0x1A, (byte) 0xE1}};
        FILE_SIGNATURE_MAP.put("VSD", extVSD);
        // VXD ['Windows virtual device drivers']
        byte[][] extVXD = {{0x4D, 0x5A}};
        FILE_SIGNATURE_MAP.put("VXD", extVXD);
        // WAB ['Outlook address file','Outlook Express address book (Win95)']
        byte[][] extWAB = {{(byte) 0x9C, (byte) 0xCB, (byte) 0xCB, (byte) 0x8D, 0x13, 0x75, (byte) 0xD2, 0x11}, {(byte) 0x81, 0x32, (byte) 0x84, (byte) 0xC1, (byte) 0x85, 0x05, (byte) 0xD0, 0x11}};
        FILE_SIGNATURE_MAP.put("WAB", extWAB);
        // WAV ['Resource Interchange File Format']
        byte[][] extWAV = {{0x52, 0x49, 0x46, 0x46}};
        FILE_SIGNATURE_MAP.put("WAV", extWAV);
        // WB2 ['QuattroPro spreadsheet']
        byte[][] extWB2 = {{0x00, 0x00, 0x02, 0x00}};
        FILE_SIGNATURE_MAP.put("WB2", extWB2);
        // WB3 ['Quatro Pro for Windows 7.0']
        byte[][] extWB3 = {{0x3E, 0x00, 0x03, 0x00, (byte) 0xFE, (byte) 0xFF, 0x09, 0x00, 0x06}};
        FILE_SIGNATURE_MAP.put("WB3", extWB3);
        // WIZ ['Microsoft Office document']
        byte[][] extWIZ = {{(byte) 0xD0, (byte) 0xCF, 0x11, (byte) 0xE0, (byte) 0xA1, (byte) 0xB1, 0x1A, (byte) 0xE1}};
        FILE_SIGNATURE_MAP.put("WIZ", extWIZ);
        // WK1 ['Lotus 1-2-3 (v1)']
        byte[][] extWK1 = {{0x00, 0x00, 0x02, 0x00, 0x06, 0x04, 0x06, 0x00}};
        FILE_SIGNATURE_MAP.put("WK1", extWK1);
        // WK3 ['Lotus 1-2-3 (v3)']
        byte[][] extWK3 = {{0x00, 0x00, 0x1A, 0x00, 0x00, 0x10, 0x04, 0x00}};
        FILE_SIGNATURE_MAP.put("WK3", extWK3);
        // WK4 ['Lotus 1-2-3 (v4']
        byte[][] extWK4 = {{0x00, 0x00, 0x1A, 0x00, 0x02, 0x10, 0x04, 0x00}};
        FILE_SIGNATURE_MAP.put("WK4", extWK4);
        // WK5 ['Lotus 1-2-3 (v4']
        byte[][] extWK5 = {{0x00, 0x00, 0x1A, 0x00, 0x02, 0x10, 0x04, 0x00}};
        FILE_SIGNATURE_MAP.put("WK5", extWK5);
        // WKS ['DeskMate Worksheet','Works for Windows spreadsheet']
        byte[][] extWKS = {{0x0E, 0x57, 0x4B, 0x53}, {(byte) 0xFF, 0x00, 0x02, 0x00, 0x04, 0x04, 0x05, 0x54}};
        FILE_SIGNATURE_MAP.put("WKS", extWKS);
        // WMA ['Windows Media Audio']
        byte[][] extWMA = {{0x30, 0x26, (byte) 0xB2, 0x75, (byte) 0x8E, 0x66, (byte) 0xCF, 0x11}};
        FILE_SIGNATURE_MAP.put("WMA", extWMA);
        // WMF ['Windows graphics metafile']
        byte[][] extWMF = {{(byte) 0xD7, (byte) 0xCD, (byte) 0xC6, (byte) 0x9A}};
        FILE_SIGNATURE_MAP.put("WMF", extWMF);
        // WMV ['Windows Media Audio']
        byte[][] extWMV = {{0x30, 0x26, (byte) 0xB2, 0x75, (byte) 0x8E, 0x66, (byte) 0xCF, 0x11}};
        FILE_SIGNATURE_MAP.put("WMV", extWMV);
        // WMZ ['Windows Media compressed skin file']
        byte[][] extWMZ = {{0x50, 0x4B, 0x03, 0x04}};
        FILE_SIGNATURE_MAP.put("WMZ", extWMZ);
        // WP ['WordPerfect text and graphics']
        byte[][] extWP = {{(byte) 0xFF, 0x57, 0x50, 0x43}};
        FILE_SIGNATURE_MAP.put("WP", extWP);
        // WP5 ['WordPerfect text and graphics']
        byte[][] extWP5 = {{(byte) 0xFF, 0x57, 0x50, 0x43}};
        FILE_SIGNATURE_MAP.put("WP5", extWP5);
        // WP6 ['WordPerfect text and graphics']
        byte[][] extWP6 = {{(byte) 0xFF, 0x57, 0x50, 0x43}};
        FILE_SIGNATURE_MAP.put("WP6", extWP6);
        // WPD ['WordPerfect text and graphics']
        byte[][] extWPD = {{(byte) 0xFF, 0x57, 0x50, 0x43}};
        FILE_SIGNATURE_MAP.put("WPD", extWPD);
        // WPF ['WordPerfect text']
        byte[][] extWPF = {{(byte) 0x81, (byte) 0xCD, (byte) 0xAB}};
        FILE_SIGNATURE_MAP.put("WPF", extWPF);
        // WPG ['WordPerfect text and graphics']
        byte[][] extWPG = {{(byte) 0xFF, 0x57, 0x50, 0x43}};
        FILE_SIGNATURE_MAP.put("WPG", extWPG);
        // WPL ['Windows Media Player playlist']
        byte[][] extWPL = {{0x4D, 0x69, 0x63, 0x72, 0x6F, 0x73, 0x6F, 0x66, 0x74, 0x20, 0x57, 0x69, 0x6E, 0x64, 0x6F, 0x77, 0x73, 0x20, 0x4D, 0x65, 0x64, 0x69, 0x61, 0x20, 0x50, 0x6C, 0x61, 0x79, 0x65, 0x72, 0x20, 0x2D, 0x2D, 0x20}};
        FILE_SIGNATURE_MAP.put("WPL", extWPL);
        // WPP ['WordPerfect text and graphics']
        byte[][] extWPP = {{(byte) 0xFF, 0x57, 0x50, 0x43}};
        FILE_SIGNATURE_MAP.put("WPP", extWPP);
        // WPS ['MSWorks text document']
        byte[][] extWPS = {{(byte) 0xD0, (byte) 0xCF, 0x11, (byte) 0xE0, (byte) 0xA1, (byte) 0xB1, 0x1A, (byte) 0xE1}};
        FILE_SIGNATURE_MAP.put("WPS", extWPS);
        // WRI ['MS Write file_1','MS Write file_2','MS Write file_3']
        byte[][] extWRI = {{0x31, (byte) 0xBE}, {0x32, (byte) 0xBE}, {(byte) 0xBE, 0x00, 0x00, 0x00, (byte) 0xAB}};
        FILE_SIGNATURE_MAP.put("WRI", extWRI);
        // WS ['WordStar Version 5.0']
        byte[][] extWS = {{0x1D, 0x7D}};
        FILE_SIGNATURE_MAP.put("WS", extWS);
        // WS2 ['WordStar for Windows file']
        byte[][] extWS2 = {{0x57, 0x53, 0x32, 0x30, 0x30, 0x30}};
        FILE_SIGNATURE_MAP.put("WS2", extWS2);
        // XDR ['BizTalk XML-Data Reduced Schema']
        byte[][] extXDR = {{0x3C}};
        FILE_SIGNATURE_MAP.put("XDR", extXDR);
        // XLA ['Microsoft Office document']
        byte[][] extXLA = {{(byte) 0xD0, (byte) 0xCF, 0x11, (byte) 0xE0, (byte) 0xA1, (byte) 0xB1, 0x1A, (byte) 0xE1}};
        FILE_SIGNATURE_MAP.put("XLA", extXLA);
        // XLS ['Microsoft Office document','Excel spreadsheet subheader_1','Excel spreadsheet subheader_2','Excel spreadsheet subheader_3','Excel spreadsheet subheader_4','Excel spreadsheet subheader_5','Excel spreadsheet subheader_6','Excel spreadsheet subheader_7']
        byte[][] extXLS = {{(byte) 0xD0, (byte) 0xCF, 0x11, (byte) 0xE0, (byte) 0xA1, (byte) 0xB1, 0x1A, (byte) 0xE1}, {0x09, 0x08, 0x10, 0x00, 0x00, 0x06, 0x05, 0x00}, {(byte) 0xFD, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF, 0x10}, {(byte) 0xFD, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF, 0x1F}, {(byte) 0xFD, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF, 0x22}, {(byte) 0xFD, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF, 0x23}, {(byte) 0xFD, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF, 0x28}, {(byte) 0xFD, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF, 0x29}};
        FILE_SIGNATURE_MAP.put("XLS", extXLS);
        // XLSX ['MS Office Open XML Format Document','MS Office 2007 documents']
        byte[][] extXLSX = {{0x50, 0x4B, 0x03, 0x04}, {0x50, 0x4B, 0x03, 0x04, 0x14, 0x00, 0x06, 0x00}};
        FILE_SIGNATURE_MAP.put("XLSX", extXLSX);
        // XML ['User Interface Language']
        byte[][] extXML = {{0x3C, 0x3F, 0x78, 0x6D, 0x6C, 0x20, 0x76, 0x65, 0x72, 0x73, 0x69, 0x6F, 0x6E, 0x3D, 0x22, 0x31, 0x2E, 0x30, 0x22, 0x3F, 0x3E}};
        FILE_SIGNATURE_MAP.put("XML", extXML);
        // XPI ['Mozilla Browser Archive']
        byte[][] extXPI = {{0x50, 0x4B, 0x03, 0x04}};
        FILE_SIGNATURE_MAP.put("XPI", extXPI);
        // XPS ['XML paper specification file']
        byte[][] extXPS = {{0x50, 0x4B, 0x03, 0x04}};
        FILE_SIGNATURE_MAP.put("XPS", extXPS);
        // XPT ['eXact Packager Models','XPCOM libraries']
        byte[][] extXPT = {{0x50, 0x4B, 0x03, 0x04}, {0x58, 0x50, 0x43, 0x4F, 0x4D, 0x0A, 0x54, 0x79}};
        FILE_SIGNATURE_MAP.put("XPT", extXPT);
        // ZAP ['ZoneAlam data file']
        byte[][] extZAP = {{0x4D, 0x5A, (byte) 0x90, 0x00, 0x03, 0x00, 0x00, 0x00, 0x04, 0x00, 0x00, 0x00, (byte) 0xFF, (byte) 0xFF}};
        FILE_SIGNATURE_MAP.put("ZAP", extZAP);
        // ZIP ['PKZIP archive_1','PKLITE archive','PKSFX self-extracting archive','PKZIP archive_2','PKZIP archive_3','WinZip compressed archive','ZLock Pro encrypted ZIP']
        byte[][] extZIP = {{0x50, 0x4B, 0x03, 0x04}, {0x50, 0x4B, 0x4C, 0x49, 0x54, 0x45}, {0x50, 0x4B, 0x53, 0x70, 0x58}, {0x50, 0x4B, 0x05, 0x06}, {0x50, 0x4B, 0x07, 0x08}, {0x57, 0x69, 0x6E, 0x5A, 0x69, 0x70}, {0x50, 0x4B, 0x03, 0x04, 0x14, 0x00, 0x01, 0x00}};
        FILE_SIGNATURE_MAP.put("ZIP", extZIP);
        // ZOO ['ZOO compressed archive']
        byte[][] extZOO = {{0x5A, 0x4F, 0x4F, 0x20}};
        FILE_SIGNATURE_MAP.put("ZOO", extZOO);
    }

    /**
     * 文件头检查
     *
     * @param file 文件
     * @return 0 不符合 1 符合 -1 未找到
     */
    public static int check(File file) throws IOException {
        final String fileName = file.getName().toUpperCase();
        final int i = fileName.lastIndexOf(".");
        String suffix = i == -1 ? fileName : fileName.substring(i + 1);
        byte[][] fileSignatureBytes = FILE_SIGNATURE_MAP.get(suffix);
        if (Objects.isNull(fileSignatureBytes)) {
            String extTARpointZ = "TAR.Z";
            if (fileName.endsWith(extTARpointZ)) {
                fileSignatureBytes = FILE_SIGNATURE_MAP.get(extTARpointZ);
            }
        }
        if (Objects.isNull(fileSignatureBytes)) {
            return -1;
        }
        for (byte[] sigBytes : fileSignatureBytes) {
            final int length = sigBytes.length;
            final byte[] fileBytes;
            try (FileInputStream fileInputStream = new FileInputStream(file)) {
                fileBytes = new byte[length];
                fileInputStream.read(fileBytes);
            }
            if (Arrays.equals(fileBytes, sigBytes)) {
                return 1;
            }
        }
        return 0;
    }
}
