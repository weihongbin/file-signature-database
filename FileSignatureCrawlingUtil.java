import cn.hutool.core.io.FileUtil;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

/**
 * 文件签名爬取工具
 * 使用依赖
 * org.jsoup:jsoup:1.13.1
 * cn.hutool:hutool-all:5.5.8
 *
 * @author WeiHongBin
 */
public class FileSignatureCrawlingUtil {
    public static void main(String[] args) throws IOException {
        StringBuilder content = new StringBuilder();
        content.append("| Extension | Signature | Description |\n");
        content.append("| :-- | :-- | :-- |\n");
        for (int page = 1; page <= 18; page++) {
            Document doc = Jsoup.connect("https://filesignatures.net/index.php?page=all&currentpage=" + page + "&order=EXT&alpha=All").get();
            Elements trElements = doc.select("table#innerTable tr");
            for (int i = 1; i < trElements.size(); i++) {
                final Elements td = trElements.get(i).select("td");
                content.append("| ");
                for (int i1 = 1; i1 < td.size(); i1++) {
                    content.append(td.get(i1).text()).append(" |");
                    if (i1 != td.size() - 1) {
                        content.append(" ");
                    }
                }
                content.append("\n");
            }
        }
        FileUtil.writeString(content.toString(), "FileSignatureDatabase.md", StandardCharsets.UTF_8);
    }
}
